package edu.cmu.cs.ziy.util.service;

import java.io.IOException;

import com.google.gson.Gson;

public class GsonRequestService implements CloseableStringService {

  protected static Gson gson = new Gson();

  protected CloseableStringService service;

  public GsonRequestService(CloseableStringService service) {
    this.service = service;
  }

  @Override
  public String postRequest(String request) throws IOException {
    return service.postRequest(request);
  }

  @Override
  public void close() throws IOException {
    service.close();
  }

  @Override
  public String getUrl() {
    return service.getUrl();
  }

}