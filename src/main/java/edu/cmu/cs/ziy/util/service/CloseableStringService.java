package edu.cmu.cs.ziy.util.service;

import java.io.Closeable;
import java.io.IOException;

public interface CloseableStringService extends Closeable {

  public abstract String postRequest(String request) throws IOException;

  public abstract String getUrl();

}