package edu.cmu.cs.ziy.util.service;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.tmatesoft.sqljet.core.SqlJetException;

import com.google.common.collect.Iterables;

import edu.cmu.cs.ziy.util.cache.SqliteStringCacher;

public class CachedService implements CloseableStringService {

  private CloseableStringService service;

  private SqliteStringCacher cacher;

  public CachedService(CloseableStringService service, File dbFile) throws SqlJetException {
    this.service = service;
    if (dbFile.exists()) {
      cacher = SqliteStringCacher.open(dbFile);
    } else {
      cacher = SqliteStringCacher.create(dbFile, SqliteStringCacher.Type.TEXT,
              SqliteStringCacher.Type.TEXT);
    }
  }

  @Override
  public String postRequest(String request) throws IOException {
    try {
      List<String> results = cacher.lookup(service.getUrl(), request);
      if (results.size() == 0) {
        String responce = service.postRequest(request);
        cacher.insert(responce, service.getUrl(), request);
        return responce;
      } else {
        return Iterables.getOnlyElement(results);
      }
    } catch (SqlJetException e) {
      throw new IOException(e);
    }
  }

  @Override
  public void close() throws IOException {
    service.close();
    try {
      cacher.close();
    } catch (SqlJetException e) {
      throw new IOException(e);
    }
  }

  @Override
  public String getUrl() {
    return service.getUrl();
  }

}
