package edu.cmu.lti.oaqa.bio.bioasq.services;

import java.io.IOException;

public class SessionUrlTimeoutRefresher implements SessionUrlSupplier {

  private long sessionTimeout;

  private String sessionUrl;

  private long sessionUrlFetchTime;

  private SessionUrlSupplier sessionUrlGetter;

  public SessionUrlTimeoutRefresher(long sessionTimeout, SessionUrlSupplier sessionUrlGetter) {
    this.sessionTimeout = sessionTimeout;
    this.sessionUrlGetter = sessionUrlGetter;
  }

  public String getSessionUrl() throws IOException {
    if (sessionUrl == null || System.currentTimeMillis() - sessionUrlFetchTime > sessionTimeout) {
      sessionUrl = sessionUrlGetter.getSessionUrl();
      sessionUrlFetchTime = System.currentTimeMillis();
    }
    return sessionUrl;
  }

}
