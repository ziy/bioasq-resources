package edu.cmu.lti.oaqa.bio.bioasq.services;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import org.apache.http.client.ClientProtocolException;
import org.tmatesoft.sqljet.core.SqlJetException;

import com.google.common.collect.ImmutableMap;

import edu.cmu.cs.ziy.util.service.CachedService;
import edu.cmu.cs.ziy.util.service.GsonRequestService;

public class LinkedLifeDataService extends GsonRequestService {

  private static final String LINKED_LIFE_DATA_SERVER = "http://gopubmed.org/web/bioasq/linkedlifedata2/triples";

  public LinkedLifeDataService() throws MalformedURLException, IOException {
    super(new GoPubMedService(new URL(LINKED_LIFE_DATA_SERVER)));
  }

  public LinkedLifeDataService(File dbFile) throws MalformedURLException, IOException,
          SqlJetException {
    super(new CachedService(new GoPubMedService(new URL(LINKED_LIFE_DATA_SERVER)), dbFile));
  }

  public LinkedLifeDataServiceResponse.Result findEntitiesPaged(String keywords, int page,
          int entitiesPerPage) throws ClientProtocolException, IOException {
    Map<String, Object[]> request = ImmutableMap.<String, Object[]> builder()
            .put("findEntitiesPaged", new Object[] { keywords, page, entitiesPerPage }).build();
    String response = postRequest(gson.toJson(request));
    return gson.fromJson(response, LinkedLifeDataServiceResponse.class).getResult();
  }

}
