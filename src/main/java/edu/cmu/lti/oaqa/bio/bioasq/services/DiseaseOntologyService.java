package edu.cmu.lti.oaqa.bio.bioasq.services;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.tmatesoft.sqljet.core.SqlJetException;

import edu.cmu.cs.ziy.util.service.CachedService;

public class DiseaseOntologyService extends OntologyService {

  private static final String DISEASE_ONTOLOGY_SERVER = "http://gopubmed.org/web/bioasq/doid/json";

  public DiseaseOntologyService() throws MalformedURLException, IOException {
    super(new GoPubMedService(new URL(DISEASE_ONTOLOGY_SERVER)));
  }

  public DiseaseOntologyService(File dbFile) throws MalformedURLException, IOException,
          SqlJetException {
    super(new CachedService(new GoPubMedService(new URL(DISEASE_ONTOLOGY_SERVER)), dbFile));
  }

}
