package edu.cmu.lti.oaqa.bio.bioasq.services;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.tmatesoft.sqljet.core.SqlJetException;

import edu.cmu.cs.ziy.util.service.CachedService;

public class UniProtService extends OntologyService {

  private static final String UNIPROT_SERVER = "http://gopubmed.org/web/bioasq/uniprot/json";

  public UniProtService() throws MalformedURLException, IOException {
    super(new GoPubMedService(new URL(UNIPROT_SERVER)));
  }

  public UniProtService(File dbFile) throws MalformedURLException, IOException, SqlJetException {
    super(new CachedService(new GoPubMedService(new URL(UNIPROT_SERVER)), dbFile));
  }

}
