package edu.cmu.lti.oaqa.bio.bioasq.services;

import java.io.IOException;
import java.util.Map;

import org.apache.http.client.ClientProtocolException;

import com.google.common.collect.ImmutableMap;

import edu.cmu.cs.ziy.util.service.CloseableStringService;
import edu.cmu.cs.ziy.util.service.GsonRequestService;

public class OntologyService extends GsonRequestService {

  protected OntologyService(CloseableStringService service) {
    super(service);
  }

  public OntologyServiceResponse.Result findEntitiesPaged(String keywords, int page,
          int conceptsPerPage) throws ClientProtocolException, IOException {
    Map<String, Object[]> request = ImmutableMap.<String, Object[]> builder()
            .put("findEntitiesPaged", new Object[] { keywords, page, conceptsPerPage }).build();
    String response = service.postRequest(gson.toJson(request));
    return gson.fromJson(response, OntologyServiceResponse.class).getResult();
  }

}
