package edu.cmu.lti.oaqa.bio.bioasq.services;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;

import org.apache.http.client.ClientProtocolException;
import org.tmatesoft.sqljet.core.SqlJetException;

import com.google.common.collect.ImmutableMap;

import edu.cmu.cs.ziy.util.service.CachedService;
import edu.cmu.cs.ziy.util.service.GsonRequestService;

public class PubMedSearchService extends GsonRequestService {

  private static final String PUBMED_SEARCH_SERVER = "http://gopubmed.org/web/gopubmedbeta/bioasq/pubmed";

  public PubMedSearchService() throws MalformedURLException, IOException {
    super(new GoPubMedService(new URL(PUBMED_SEARCH_SERVER)));
  }

  public PubMedSearchService(File dbFile) throws MalformedURLException, IOException,
          SqlJetException {
    super(new CachedService(new GoPubMedService(new URL(PUBMED_SEARCH_SERVER)), dbFile));
  }

  public PubMedSearchServiceResponse.Result findPubMedCitations(String keywords, int page,
          int articlesPerPage) throws ClientProtocolException, IOException {
    Map<String, Object[]> request = ImmutableMap.<String, Object[]> builder()
            .put("findPubMedCitations", new Object[] { keywords, page, articlesPerPage }).build();
    String response = postRequest(gson.toJson(request));
    return gson.fromJson(response, PubMedSearchServiceResponse.class).getResult();
  }

}
