package edu.cmu.lti.oaqa.bio.bioasq.services;

import java.io.IOException;

public interface SessionUrlSupplier {

  String getSessionUrl() throws IOException;

}
