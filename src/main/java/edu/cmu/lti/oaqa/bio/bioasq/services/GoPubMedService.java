package edu.cmu.lti.oaqa.bio.bioasq.services;

import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

import org.apache.http.HttpEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;

import com.google.common.base.Charsets;
import com.google.common.io.CharStreams;

import edu.cmu.cs.ziy.util.service.CloseableStringService;

public class GoPubMedService implements CloseableStringService {

  protected CloseableHttpClient client = HttpClients.createSystem();

  private URL baseUrl;

  private SessionUrlTimeoutRefresher sessionUrlRefetcher;

  public GoPubMedService(final URL baseUrl) throws IOException {
    this.baseUrl = baseUrl; 
    this.sessionUrlRefetcher = new SessionUrlTimeoutRefresher(550000, new SessionUrlSupplier() {

      @Override
      public String getSessionUrl() throws IOException {
        HttpPost post = new HttpPost(baseUrl.toString());
        CloseableHttpResponse response = client.execute(post);
        return CharStreams.toString(new InputStreamReader(response.getEntity().getContent(),
                Charsets.UTF_8));
      }
    });
  }

  @Override
  public String postRequest(String jsonString) throws IOException {
    HttpEntity entity = MultipartEntityBuilder
            .create()
            .addTextBody("json", jsonString, ContentType.create("application/json", Charsets.UTF_8))
            .build();
    HttpPost post = new HttpPost(sessionUrlRefetcher.getSessionUrl());
    post.setEntity(entity);
    CloseableHttpResponse response = client.execute(post);
    try {
      return CharStreams.toString(new InputStreamReader(response.getEntity().getContent(),
              Charsets.UTF_8));
    } finally {
      response.close();
    }
  }

  @Override
  public void close() throws IOException {
    client.close();
  }

  @Override
  public String getUrl() {
    return baseUrl.toString();
  }

}