package edu.cmu.lti.oaqa.bio.bioasq.services;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.tmatesoft.sqljet.core.SqlJetException;

import edu.cmu.cs.ziy.util.service.CachedService;

public class MeshService extends OntologyService {

  private static final String MESH_SERVER = "http://gopubmed.org/web/bioasq/mesh/json";

  public MeshService() throws MalformedURLException, IOException {
    super(new GoPubMedService(new URL(MESH_SERVER)));
  }

  public MeshService(File dbFile) throws MalformedURLException, IOException, SqlJetException {
    super(new CachedService(new GoPubMedService(new URL(MESH_SERVER)), dbFile));
  }

}
