package edu.cmu.lti.oaqa.bio.bioasq.submission;

public enum QuestionType {
  factoid, list, yesno, summary
}