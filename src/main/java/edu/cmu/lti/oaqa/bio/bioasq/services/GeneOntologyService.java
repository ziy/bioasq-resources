package edu.cmu.lti.oaqa.bio.bioasq.services;

import java.io.File;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.tmatesoft.sqljet.core.SqlJetException;

import edu.cmu.cs.ziy.util.service.CachedService;

public class GeneOntologyService extends OntologyService {

  private static final String GENE_ONTOLOGY_SERVER = "http://gopubmed.org/web/bioasq/go/json";

  public GeneOntologyService() throws MalformedURLException, IOException {
    super(new GoPubMedService(new URL(GENE_ONTOLOGY_SERVER)));
  }

  public GeneOntologyService(File dbFile) throws MalformedURLException, IOException,
          SqlJetException {
    super(new CachedService(new GoPubMedService(new URL(GENE_ONTOLOGY_SERVER)), dbFile));
  }

}
