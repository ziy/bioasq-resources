package edu.cmu.lti.oaqa.bio.bioasq.services;

import static org.junit.Assert.assertEquals;

import java.io.InputStreamReader;
import java.io.Reader;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.gson.Gson;

public class PubMedSearchServiceResponseTest {

  private static Gson gson;

  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    gson = new Gson();
  }

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
  }

  @Test
  public void testGetResult() {
    Reader reader = new InputStreamReader(getClass().getResourceAsStream(
            "/services_examples/PubMedSearchServiceCall.example.response.json"));
    PubMedSearchServiceResponse json = gson.fromJson(reader, PubMedSearchServiceResponse.class);
    assertEquals(json.getResult().getDocuments().size(), 10);
  }
}
