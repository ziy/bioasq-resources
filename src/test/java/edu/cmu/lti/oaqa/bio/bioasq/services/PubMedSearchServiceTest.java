package edu.cmu.lti.oaqa.bio.bioasq.services;

import static org.junit.Assert.assertEquals;

import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import edu.cmu.lti.oaqa.bio.bioasq.services.PubMedSearchServiceResponse.Result;

public class PubMedSearchServiceTest {

  private static PubMedSearchService service;

  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    service = new PubMedSearchService();
  }

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
    service.close();
  }

  @Test
  public void testFindEntitiesPaged() {
    try {
      Result result = service.findPubMedCitations("nitric oxide synthase", 0, 10);
      assertEquals(result.getDocuments().size(), 10);
    } catch (ClientProtocolException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

}
