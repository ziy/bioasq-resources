package edu.cmu.lti.oaqa.bio.bioasq.services;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import org.apache.http.client.ClientProtocolException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import edu.cmu.lti.oaqa.bio.bioasq.services.OntologyServiceResponse.Result;

public class GeneOntologyServiceTest {

  private static GeneOntologyService service;

  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    service = new GeneOntologyService();
  }

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
    service.close();
  }

  @Test
  public void testFindEntitiesPaged() {
    try {
      Result result = service.findEntitiesPaged("nitric oxide synthase", 0, 10);
      assertEquals(result.getFindings().size(), 10);
      System.out.println("0");
      try {
        TimeUnit.SECONDS.sleep(1);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      result = service.findEntitiesPaged("nitric oxide synthase", 0, 10);
      assertEquals(result.getFindings().size(), 10);
      System.out.println("1 second");
      try {
        TimeUnit.MINUTES.sleep(9);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      result = service.findEntitiesPaged("nitric oxide synthase", 0, 10);
      assertEquals(result.getFindings().size(), 10);
      System.out.println("9 min");
      try {
        TimeUnit.MINUTES.sleep(1);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      result = service.findEntitiesPaged("nitric oxide synthase", 0, 10);
      assertEquals(result.getFindings().size(), 10);
      System.out.println("10 min");
      try {
        TimeUnit.MINUTES.sleep(1);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      result = service.findEntitiesPaged("nitric oxide synthase", 0, 10);
      assertEquals(result.getFindings().size(), 10);
      System.out.println("11 min");
    } catch (ClientProtocolException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

}
