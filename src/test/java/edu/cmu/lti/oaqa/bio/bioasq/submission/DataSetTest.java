package edu.cmu.lti.oaqa.bio.bioasq.submission;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.io.InputStream;
import java.io.StringReader;
import java.util.List;
import java.util.Set;

import org.junit.Test;

import com.google.common.collect.Sets;

public class DataSetTest {

  @Test
  public void testLoadPhaseATestSet() {
    InputStream stream = getClass().getResourceAsStream(
            "/submission_examples/BioASQ-task2bPhaseA-testset1");
    List<? extends Question> dataset = TestSet.load(stream);
    assertEquals(dataset.size(), 4);
    Set<QuestionType> types = Sets.newHashSet();
    for (Question question : dataset) {
      types.add(question.getType());
    }
    assertEquals(types.size(), 4);
  }

  @Test
  public void testLoadPhaseBTestSet() {
    InputStream stream = getClass().getResourceAsStream(
            "/submission_examples/BioASQ-task2bPhaseB-testset1");
    List<? extends Question> dataset = TestSet.load(stream);
    assertEquals(dataset.size(), 4);
    Set<QuestionType> types = Sets.newHashSet();
    for (Question question : dataset) {
      types.add(question.getType());
    }
    assertEquals(types.size(), 4);
    assertNull(dataset.get(0).getTriples());
    assertNotNull(dataset.get(2).getTriples());
  }

  @Test
  public void testLoadPhaseAResult() {
    InputStream stream = getClass().getResourceAsStream(
            "/submission_examples/results_2b_phaseA.json");
    List<? extends Question> dataset = TestSet.load(stream);
    assertEquals(dataset.size(), 4);
  }

  @Test
  public void testLoadPhaseBResult() {
    InputStream stream = getClass().getResourceAsStream(
            "/submission_examples/results_2b_phaseB.json");
    List<? extends Question> dataset = TestSet.load(stream);
    assertEquals(dataset.size(), 4);
  }

  @Test
  public void testLoadTrainingSet() {
    InputStream stream = getClass().getResourceAsStream(
            "/submission_examples/BioASQ-SampleData1B.json");
    List<? extends Question> dataset = TestSet.load(stream);
    assertEquals(dataset.size(), 29);
    for (Question question : dataset) {
      if (question instanceof TestFactoidQuestion) {
        assertNotNull(((TestFactoidQuestion) question).getExactAnswer());
      } else if (question instanceof TestYesNoQuestion) {
        assertNotNull(((TestYesNoQuestion) question).getExactAnswer());
      } else if (question instanceof TestListQuestion) {
        assertNotNull(((TestListQuestion) question).getExactAnswer());
      }
    }
  }

  @Test
  public void testDumpPhaseAResult() {
    InputStream stream = getClass().getResourceAsStream(
            "/submission_examples/results_2b_phaseA.json");
    List<? extends TestQuestion> origDataset = TestSet.load(stream);
    String output = TestSet.dump(origDataset);
    List<? extends TestQuestion> dumpedDataset = TestSet.load(new StringReader(output));
    assertEquals(origDataset, dumpedDataset);
  }

  @Test
  public void testDumpPhaseBResult() {
    InputStream stream = getClass().getResourceAsStream(
            "/submission_examples/results_2b_phaseB.json");
    List<? extends TestQuestion> origDataset = TestSet.load(stream);
    String output = TestSet.dump(origDataset);
    List<? extends TestQuestion> dumpedDataset = TestSet.load(new StringReader(output));
    assertEquals(origDataset, dumpedDataset);
  }

}
