package edu.cmu.cs.ziy.util.service;

import static org.junit.Assert.assertEquals;

import java.io.File;
import java.io.IOException;

import org.apache.http.client.ClientProtocolException;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import edu.cmu.lti.oaqa.bio.bioasq.services.GeneOntologyService;
import edu.cmu.lti.oaqa.bio.bioasq.services.OntologyServiceResponse.Result;

public class CachedServiceTest {

  private static GeneOntologyService service;

  @BeforeClass
  public static void setUpBeforeClass() throws Exception {
    File dbFile = new File("src/test/resources/cache/cache.db3");
    service = new GeneOntologyService(dbFile);
  }

  @AfterClass
  public static void tearDownAfterClass() throws Exception {
    service.close();
  }

  @Test
  public void testFindEntitiesPaged() {
    try {
      Result result = service.findEntitiesPaged("nitric oxide synthase", 0, 10);
      assertEquals(result.getFindings().size(), 10);
    } catch (ClientProtocolException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

}
